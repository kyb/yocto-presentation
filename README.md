---
marp: true
title: Yocto Project
description: Introduce Yocto Project to LPD team
theme: uncover
class:
  - lead
  - invert
paginate: true
_paginate: false
---

# Yocto Project
presentation for 
## private company


<!-- *page_number: false -->

![](yocto-logo.png)  

by Ivan Kuvaldin ( [@kuvaldini](https://github.com/kuvaldini) )

---

<!--All right, see, you have a mission to develop world's first operation system for...-->
[![bg fit](why-yocto.png)](The_Yocto_Project-utZpKM7i5Z4.webm "The Yocto Project")
[![](alpha.png)](The_Yocto_Project-utZpKM7i5Z4.webm "The Yocto Project")

---

### Why yocto?
* Universal and poppular solution to create a bundle of Linux-based software.
* Handles full software development cycle from sources to prebuilt SDK and then to final firmware.
* Provides clean package and depedancy management.
* Keep toolchains, kernel, drivers and applications up to date and in sync with upstream repositories.
* Automated build system with cache

---

#### [How it works](https://www.yoctoproject.org/software-overview/)
[![bg fit](yp-how-it-works-new-diagram.play.png)](YoctoProject_How_it_works--cXvE4VyyA8.mp4)
[![](alpha.png)](YoctoProject_How_it_works--cXvE4VyyA8.mp4 "The Yocto Project - How it works")

---
![bg](rgb(73,27,111))
## Components
* poky is a main repo: includes BitBake and basic layers `meta-*`
* custom layers `meta-our-pcb` and `meta-our-sw` describe hardware platforms and software bundles (to be written)
* non-yocto software sources: our-sw, third-party.

---
![bg](rgb(13,72,33))
## Outputs
* Repository of prebuild packages: `rpm`, `deb`, `ipk`. Each package has at least two parts: a) executable and/or library, b) stripped debug symbols.
* Application development SDK: target architecture root file system (sysroot), contains unpacked packages.
* Binary firmware, may be several configurations: minimal for recovery and full for operation

---
![bg](bg.jpg)

# The Plan
* Take Ambarella SDK as is, as a base component. Write recepies to build image using BitBake.
* Rip up AmbaSDK part by part, begin from two ends: <-) linux kernel, (-> prebuilt apps and libs. Smoothly transfer responsibility to BitBake.
* Update kernel. Keep Linux kernel clean, apply Ambarella and our patches with BitBake recepies during build.

---
![bg](bg.jpg)

## The Plan (continue)
* Write a recepies for all libs and apps to be built from sources.
* Use Toaster - Yocto automated build system.
* Remake update system to use package manager.

---

### Final profits

* Minify firmware size: we need to run about ten services, and wanna keep only dependancies of that applications, other is trash. 
 **Can save may be 30% of disk space.**
* Able to **release** software **often: rolling-release**, update package by package
* Every subproject of LPD-FW can be built and tested apart from overall repo. **Increase build and test speed**

---
### Final profits (continue)

* Own source code of every component, able to make changes, reconfigure, apply patches to third-party content.
* One step closer to create DEBUG environment: **one click GDB from IDE**. *Will have a normal sysroot with configured toolchain*

---

## Profits for development flow

* Minimize projects binding - every team is responsible for its repo, not full firmware.
* Significantly Faster CI: build and test sw packages, not full firmware.
* Conviniency for developers and testers: able to change target system configuration without reflashing.







---------------------------------------------

### Useful links
1. [Yocto Overview manual](https://www.yoctoproject.org/docs/2.7/overview-manual/overview-manual.html)
2. [Yocto Toaster user manual](https://www.yoctoproject.org/docs/2.7/toaster-manual/toaster-manual.html) - web interface to bitbake, like Jenkins
3. [Yocto Understanding and creating layers](https://www.yoctoproject.org/docs/2.7/dev-manual/dev-manual.html#understanding-and-creating-layers)
4. [Yocto Dev Chapter 3. Common Tasks](https://www.yoctoproject.org/docs/2.7/dev-manual/dev-manual.html#extendpoky) - the main things to get know

---

### Useful links (continue)
5. [Yocto Project Quick Build](https://www.yoctoproject.org/docs/current/brief-yoctoprojectqs/brief-yoctoprojectqs.html)
6. [Building Raspberry Pi Systems with Yocto](https://jumpnowtek.com/rpi/Raspberry-Pi-Systems-with-Yocto.html) - one guy's article about how to build for raspberry pi
7. [Yocto crops](https://github.com/crops/docker-win-mac-docs/wiki) - create build environment with Docker
8. [Yocto eSDK](https://www.yoctoproject.org/docs/2.7/sdk-manual/sdk-manual.html#sdk-extensible) - another way of build env

---

## Other projects for reference
1. [meta-balena](https://github.com/balena-os/meta-balena)
2. [Balena Custom board support](https://www.balena.io/docs/reference/OS/meta-balena/) - good Linux for IOT on raspberry pi
2. [meta-raspberrypi](https://github.com/agherzan/meta-raspberrypi)
3. [meta-rpi](https://github.com/jumpnow/meta-rpi) from [Building Raspberry Pi Systems with Yocto](https://jumpnowtek.com/rpi/Raspberry-Pi-Systems-with-Yocto.html)


---

## Slides
0. [Marp CLI example](https://yhatt-marp-cli-example.netlify.com) -- [github](https://github.com/yhatt/marp-cli-example#marp-cli-example)
1. [marp-core](https://github.com/marp-team/marp-core/tree/master/themes)
2. https://github.com/cgroll/pandoc_slides
3. https://github.com/cgroll/research_tools
4. [marp themes](https://matsubara0507-slides.netlify.com)

---
<!-- _class: uncover invert-->
![](thank-you.sjpg)
Thank you for attention!

# Yocto Project
presentation for 
## private company


<!-- *page_number: false -->

![](yocto-logo.png)  
