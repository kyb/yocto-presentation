#!/usr/bin/env bash
if marp --version &>/dev/null ;then 
   marp README.md -o index.html
else
   npx @marp-team/marp-cli README.md -o index.html
fi
