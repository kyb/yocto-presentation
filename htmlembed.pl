#!/usr/bin/env perl
use strict;
use warnings;
#use autodie;

use MIME::Base64;

sub image2base64 {
   my $filename = $_[0];
   open my $fh , "<", "$filename" or do { print STDERR "Failed to open $filename\n\n\n"; return };
   my ($file_ext) = $filename =~ /\.([^.]*)$/;
   local($/) = undef;  # slurp, to read file at ones, undefine record separator, default was \n
   my $img64 = "data:image/$file_ext;base64," . encode_base64(<$fh>);
   return $img64;
}

while(<>){
   s{<img src="(.*?)"}   {
      '<img src="' . do { my $a = image2base64($1); if(defined $a){$a}else{$1} } . '"'
   }eg;
   print;
}

### Big image files should be read by chunks.
   #open my $out64, '>', "$filename.base64";
   #while( read($fh, my $buf, 1024*32) ){
   #   print $out64 encode_base64url($buf);
   #}
